Un joc de plataformes 3D

***Scripts de control de jugador:***

PlayerControl:

Controla el moviment del jugador, el jugador es mou a partir de forces i rota depenent del moviment del ratolí.
Hi ha dos sets de físiques, un mentre el jugador està a l'aire i un altre quan està a terra, el jugador pot corregir els seus salts però amb molta pitjor eficàcia que el moviment terrestre.
Si el jugador està a terra la seva velocitat està bloquejada a un màxim de 5, aquest limitador no existeix en el aire.
CameraFollow:

Clava la càmera darrere del jugador la càmera és un fill de l'objecte del jugador per tant al girar l'objecte del jugador amb el ratolí, la càmera el seguirà.

***Scripts d'objecte***


La resta d'escripts controlen el funcionament dels objectes amb el que el jugador interactua.

KeyCollection:

Un script amb un event que es dispara si un jugador toca alguna de les claus.

ExitScript:

Control·la el canvi d'escenes i rep el event un cop el jugador recull una clau.
El jugador sempre és enviat a "Scene" + (Número d'escena actual + 1) per tal que el mateix codi pugui ser reutilitzat per a totes les escenes sense haver de canviar coses manualment.

PlatformController:

Controla el moviment de les plataformes mòbils, utilitza un scriptableobject amb la distància x, y i z que és té que moure, el temps d'arribada i el temps d'espera entre moviments.

A l'entrar en contacte amb la plataforma, el jugador es fa fill d'un objecte buit dins de la plataforma per tal que es mogui amb ella sense tindre problemes heretant valors d'escale.

Boosterscript:

Controla el funcionament del trampolí, a l'entrar en contacte genera una força de 2000 al vector up del jugador.

InverterScript:

Al entrar en contacte inverteix la gravetat de la escena (O la torna a la normalitat si ja esta invertida), també desplaça al jugador una mica per tal de que no estigui xocant amb la plataforma continuament.

EndingScript:

Mostra el text de final de joc, no te cap altra funció.

***BUGS I PROBLEMES***

-Quan una plataforma mòbil accelera, el jugador patina una mica cap enrere per l'increment de velocitat, el mateix passa al frenar.

-El sistema de partícules de la sortida de cada nivell deixa de funcionar i encalla al jugador si es fa un canvi d'escena, no he trobat el motiu per tant he desactivat aquesta línia de codi.

-El moviment de les plataformes pot ser alterat depenent del framerate del jugador, en casos molt extrems poden xocar contra altres plataformes i deixar de funcionar.
