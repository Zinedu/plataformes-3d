﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MovingPlatform : ScriptableObject
{
    public Vector3 targetCoords;
    public float movementTime;
    public float idleTime;

}
