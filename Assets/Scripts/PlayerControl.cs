﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    float maxSpeed = 7;
    float mouseX;
    float mouseY;
    bool jump = true;
    float mouseSpeed = 5;
    public bool onPlatform = false;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    // Update is called once per frame
    void Update()
    {

        mouseX += mouseSpeed * Input.GetAxis("Mouse X");
        mouseY -= mouseSpeed * Input.GetAxis("Mouse Y");

        this.gameObject.transform.eulerAngles = new Vector3(0, mouseX, 0);
        if (jump)
        {
            if (Input.GetKey(KeyCode.W))
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.forward * 15);
            }

            if (Input.GetKey(KeyCode.S))
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.forward * -15);
            }

            if (Input.GetKey(KeyCode.A))
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.right * -15);
            }

            if (Input.GetKey(KeyCode.D))
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.right * 15);
            }

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (jump)
                {
                    this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.up * 500);
                    jump = false;

                }
            }

            if (this.gameObject.GetComponent<Rigidbody>().velocity.magnitude > maxSpeed && !onPlatform)
            {
                this.gameObject.GetComponent<Rigidbody>().velocity = this.gameObject.GetComponent<Rigidbody>().velocity.normalized * maxSpeed;
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.W))
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.forward * 2);
            }

            if (Input.GetKey(KeyCode.S))
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.forward * -2);
            }

            if (Input.GetKey(KeyCode.A))
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.right * -2);
            }

            if (Input.GetKey(KeyCode.D))
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(this.gameObject.transform.right * 2);
            }
        }

        if(this.gameObject.transform.position.y < -40 || this.gameObject.transform.position.y > 100)
        {
            this.gameObject.transform.position = new Vector3(0, 2, 0);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Platform" && this.gameObject.transform.position.y > collision.gameObject.transform.position.y && !InverterScript.invertedGrav)
        {
            jump = true;
        }

        if (collision.gameObject.tag == "Platform" && this.gameObject.transform.position.y < collision.gameObject.transform.position.y && InverterScript.invertedGrav)
        {
            jump = true;
        }

    }
}
