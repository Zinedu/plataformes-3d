﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    public MovingPlatform modifier;

    bool cooldown = false;
    bool returning = false;
    Vector3 originalCoords;

    // Start is called before the first frame update
    void Start()
    {
        originalCoords = this.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        if (!cooldown)
        {

            if (!returning)
            {
                this.gameObject.GetComponent<Rigidbody>().velocity = (modifier.targetCoords / modifier.movementTime);
                if (Vector3.Distance(this.gameObject.transform.position, originalCoords + modifier.targetCoords) < 0.2)
                {
                    this.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                    cooldown = true;
                    StartCoroutine(CooldownTimer());
                }
            }
            else
            {
                this.gameObject.GetComponent<Rigidbody>().velocity = -(modifier.targetCoords / modifier.movementTime);
                if (Vector3.Distance(this.gameObject.transform.position, originalCoords) < 0.2)
                {
                    this.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                    cooldown = true;
                    StartCoroutine(CooldownTimer());
                }
            }

            
        }

    }

    IEnumerator CooldownTimer()
    {
        yield return new WaitForSeconds(modifier.idleTime);
        cooldown = false;
        if (!returning)
        {
            returning = true;
        }
        else
        {
            returning = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.transform.parent = this.gameObject.transform.GetChild(0);
            collision.gameObject.GetComponent<PlayerControl>().onPlatform = true;
            collision.gameObject.GetComponent<Rigidbody>().velocity = this.gameObject.GetComponent<Rigidbody>().velocity;

        }
    }


    private void OnCollisionExit(Collision collision)
    {
        collision.gameObject.transform.parent = null;
        collision.gameObject.GetComponent<PlayerControl>().onPlatform = false;
    }

}
