﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InverterScript : MonoBehaviour
{
    // Start is called before the first frame update
    public static bool invertedGrav = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            if (invertedGrav)
            {
                collision.gameObject.transform.position = collision.gameObject.transform.position + new Vector3(0, -2, 0);
                collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, -9.81f, 0));
            }
            else
            {
                collision.gameObject.transform.position = collision.gameObject.transform.position + new Vector3(0, 2, 0);
                collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 9.81f, 0));
            }

            if (!invertedGrav)
            {
                Physics.gravity = Vector3.up;
                invertedGrav = true;
            }
            else
            {
                Physics.gravity = -Vector3.up;
                invertedGrav = false;
            }
        }
    }
}
