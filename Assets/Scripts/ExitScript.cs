﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitScript : MonoBehaviour
{
    int keyNumber = 0;
    int keysNeeded;
    bool unlocked = false;
    byte currentSceneNumber;

    // Start is called before the first frame update
    void Start()
    {
        KeyCollection.OnContact += OnKeyCollect;
        keysNeeded = GameObject.FindGameObjectsWithTag("Key").Length;
        currentSceneNumber = (byte)int.Parse(char.ToString(SceneManager.GetActiveScene().name[SceneManager.GetActiveScene().name.Length - 1]));
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnKeyCollect()
    {
        keyNumber++;
        if (keyNumber >= keysNeeded)
        {
            unlocked = true;
            //Referenciar el sistema de particules despres d'un canvi d'escena provoca un error, no entec el motiu
            //this.gameObject.transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(unlocked && collision.gameObject.name == "PlayerObject")
        {
            SceneManager.LoadScene("Scene" + (currentSceneNumber + 1));
        }
    }
}
