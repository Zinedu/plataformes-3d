﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    GameObject playerObject;
    // Start is called before the first frame update
    void Start()
    {
        playerObject = GameObject.Find("PlayerObject");
    }

    // Update is called once per frame
    void Update()
    {
        if (!InverterScript.invertedGrav)
        {
            this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, (playerObject.transform.position - playerObject.transform.forward * 10) + playerObject.transform.up * 2, 2f);
        }
        else
        {
            this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, (playerObject.transform.position - playerObject.transform.forward * 10) + playerObject.transform.up * -2, 2f);
        }
        
        this.gameObject.transform.rotation = playerObject.transform.rotation;
    }
}
