﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingScript : MonoBehaviour
{
    GameObject endText;
    // Start is called before the first frame update
    void Start()
    {
        endText = GameObject.Find("New Text");
        endText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        endText.SetActive(true);
        GameObject.Destroy(this.gameObject);

    }
}
