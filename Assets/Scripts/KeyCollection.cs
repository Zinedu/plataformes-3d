﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCollection : MonoBehaviour
{
    // Start is called before the first frame update

    public delegate void Contact();
    public static event Contact OnContact;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "PlayerObject")
        {
            if (OnContact != null)
            {
                OnContact();
                GameObject.Destroy(this.gameObject);
            }

        }
    }
}
